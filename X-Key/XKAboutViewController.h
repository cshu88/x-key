//
//  XKAboutViewController.h
//  X-Key
//
//  Created by Yi Shen on 4/1/14.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface XKAboutViewController : UIViewController <MFMailComposeViewControllerDelegate>

- (IBAction)shareBtnPressed:(id)sender;

@end
