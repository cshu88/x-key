//
//  XKKeyListCell.m
//  X-Key
//
//  Created by Chang Shu on 3/20/14.
//
//

#import "XKKeyListCell.h"

@interface XKKeyListCell ()

@property NSMutableArray *columns;

@end
@implementation XKKeyListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)addColumn_internal:(CGFloat)position
{
    [self.columns addObject:[NSNumber numberWithFloat:position]];
}

- (void)setColumn:(int)column WithLabel:(NSString*)text
{
    if (column == 0) {
        self.textLabel.text = text;
    } else if (column > 0 && column <= [self.columns count]){ // column count does not include 1st column
        UILabel * label = self.columns[column - 1];
        label.text = text;
    }
}

- (void)initColumnNumber:(int)numberOfColumns
{
    if ([self.columns count] > 1)
        return;
    if (numberOfColumns <= 1)
        return;

    self.columns = [[NSMutableArray alloc] initWithCapacity:numberOfColumns];

    int x = self.textLabel.frame.origin.x;
    int y = self.textLabel.frame.origin.y;
    int height = self.textLabel.frame.size.height;
    int width = self.textLabel.frame.size.width / numberOfColumns;
    CGRect refFrame = CGRectMake(x, y, width, height);
    self.textLabel.frame = refFrame;

    for (int i = 1; i < numberOfColumns; i++) {
        x += width;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
//        label.tag = 1;
        label.font = self.textLabel.font;
        label.textAlignment = self.textLabel.textAlignment;
        label.textColor = [UIColor darkGrayColor];// self.textLabel.textColor;
        label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleHeight;
        [self.columns addObject:label];
        [self.contentView addSubview:label];
    }
}

- (void)drawRect:(CGRect)rect
{
    return;
/*
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    // Use the same color and width as the default cell separator for now
    CGContextSetRGBStrokeColor(ctx, 1, 1, 1, 1);
//    CGContextSetRGBStrokeColor(ctx, 0.5, 0.5, 0.5, 1.0);
    CGContextSetLineWidth(ctx, 1.0);
    
    for (int i = 0; i < [self.columns count]; i++) {
        UILabel * label = [self.columns objectAtIndex:i];
        CGFloat f = label.frame.origin.x;
        CGContextMoveToPoint(ctx, f, 0);
        CGContextAddLineToPoint(ctx, f, 40);//self.bounds.size.height);
    }
    
    CGContextStrokePath(ctx);
    
    [super drawRect:rect];
*/

}

@end
