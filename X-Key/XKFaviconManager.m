//
//  XKFaviconManager.m
//  X-Key
//
//  Created by Chang Shu on 3/25/14.
//
//

#import "XKFaviconManager.h"

#import "AppDelegate.h"
#import "XKKeyListViewController.h"

@implementation XKFaviconManager

NSString * const kFaviconFileName = @"Documents/XKFavicons.bin";
NSString * const kDefaultUrlString = @"unknown";

- (id)init
{
    self = [super init];
    _responseDataMap = CFDictionaryCreateMutable(kCFAllocatorDefault,
                                                 0,
                                                 &kCFTypeDictionaryKeyCallBacks,
                                                 &kCFTypeDictionaryValueCallBacks);
    _connectonMap = [[NSMutableDictionary alloc] init];
    _faviconMap = [[NSMutableDictionary alloc] init];
    _loadedFaviconMap = [[NSMutableDictionary alloc] init];
    _faviconMap[kDefaultUrlString] = [UIImage imageNamed:@"LogoBig.png"];
    return self;
}

- (void)setController:(id)controller
{
    _controller = controller;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [self init];
    if (!self)
        return nil;

    _loadedFaviconMap = [coder decodeObjectForKey:@"faviconmap"];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    //    [coder encodeInt:[self.keys count] forKey:kNumberOfKeys];
    [coder encodeObject:_faviconMap forKey:@"faviconmap"];
//    for (NSString * key in _faviconMap) {
//        UIImage * favicon = _faviconMap[key];
//        [coder encodeObject:favicon forKey:key];
//    }
}

- (NSData*)serializeData
{
    return [NSKeyedArchiver archivedDataWithRootObject:self];
}

+ (XKFaviconManager *)deSerializeData:(NSData*)serializedData
{
    XKFaviconManager * faviconMgr = nil;
    if (serializedData) {
        @try {
            faviconMgr = [NSKeyedUnarchiver unarchiveObjectWithData:serializedData];
            if (![faviconMgr isKindOfClass:[XKFaviconManager class]]) {
                faviconMgr = nil;
            }
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
            faviconMgr = nil;
        }
    }
    return faviconMgr;
}

+ (XKFaviconManager *)loadFavicons
{
    XKFaviconManager *faviconMgr = nil;
    NSString *faviconFilePath = [NSHomeDirectory() stringByAppendingPathComponent:kFaviconFileName];
    NSData *data = [NSData dataWithContentsOfFile:faviconFilePath];
    if (data) {
        faviconMgr = [self deSerializeData:data];
    }
    if (!faviconMgr) {
        faviconMgr = [[XKFaviconManager alloc] init];
    }
    return faviconMgr;
}

- (void)saveFavicons
{
    NSString  *faviconFilePath = [NSHomeDirectory() stringByAppendingPathComponent:kFaviconFileName];
    NSData *data = [self serializeData];
    [data writeToFile:faviconFilePath atomically:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    NSData *responseData = [[NSMutableData alloc] init];
    CFDictionaryAddValue(_responseDataMap, (__bridge const void *)(connection), (__bridge const void *)(responseData));
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    NSMutableData * responseData = CFDictionaryGetValue(_responseDataMap, (__bridge const void *)(connection));
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    NSMutableData * responseData = CFDictionaryGetValue(_responseDataMap, (__bridge const void *)(connection));
    UIImage * favicon = [[UIImage alloc] initWithData:responseData];
    for (NSString * key in _connectonMap) {
        id value = [_connectonMap objectForKey:key];
        if (value == connection) {
            _faviconMap[key] = favicon;
            XKKeyListViewController * listVC = (XKKeyListViewController*)_controller;
            [listVC.tableView reloadData];
            [_connectonMap removeObjectForKey:key];
            break;
        }
    }
    CFDictionaryRemoveValue(_responseDataMap, (__bridge const void *)(connection));
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    for (NSString * key in _connectonMap) {
        id value = [_connectonMap objectForKey:key];
        if (value == connection) {
            _faviconMap[key] = [UIImage imageNamed:@"LogoBig.png"];
            [_connectonMap removeObjectForKey:key];
            break;
        }
    }
    CFDictionaryRemoveValue(_responseDataMap, (__bridge const void *)(connection));
}

- (void)requestFavicon:(NSString *)urlString
{
#if 1
    return;
#else
    if (_connectonMap[urlString]) {
        return;
    }

    NSMutableString *requestUrlString = [[NSMutableString alloc] initWithString:@""];
    if ([urlString hasPrefix:@"http://www"] || [urlString hasPrefix:@"https://www"])
        [requestUrlString appendString:urlString];
    else if ([urlString hasPrefix:@"http"])
        [requestUrlString appendString:[NSString stringWithFormat:@"www.%@", urlString]];
    else if ([urlString hasPrefix:@"www."])
        [requestUrlString appendString:[NSString stringWithFormat:@"https://%@", urlString]];
    else
        [requestUrlString appendString:[NSString stringWithFormat:@"https://www.%@", urlString]];

    [requestUrlString appendString:@"/favicon.ico"];

    Boolean inUseSecure = true;
    NSURL *url = [NSURL URLWithString:requestUrlString];
    NSURLComponents *components = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:YES];
    components.scheme = inUseSecure ? @"https" : @"http";

#if 0
    NSURLSessionDownloadTask *downloadTask = [[NSURLSession sharedSession]
                                              downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                  UIImage *favicon = [UIImage imageWithData: [NSData dataWithContentsOfURL:location]];
                                                  _faviconMap[urlString] = favicon;
                                                  XKKeyListViewController * listVC = (XKKeyListViewController*)_controller;
                                                  [listVC.tableView reloadData];

    }];

    [downloadTask resume];
#else
    // Create the request.
    NSURLRequest *request = [NSURLRequest requestWithURL:components.URL];

    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    _connectonMap[urlString] = conn;
#endif
#endif
}

- (UIImage*)retrieveFavicon:(NSString*)urlString
{
    UIImage *favicon = nil;
    if (!urlString) {
        urlString = kDefaultUrlString;
    }
    favicon = _faviconMap[urlString];
    if (!favicon) {
        favicon = _loadedFaviconMap[urlString];
    }
    if (!favicon) {
        [self requestFavicon:urlString];
    } else {
        _faviconMap[urlString] = favicon;
    }
    return favicon;
}

- (void)removeFavicon:(NSString*)urlString
{
    if (!urlString)
        return;
    id conn = _connectonMap[urlString];
    if (conn) {
        CFDictionaryRemoveValue(_responseDataMap, (__bridge const void *)(conn));
    }
    [_connectonMap removeObjectForKey:urlString];
    [_faviconMap removeObjectForKey:urlString];
}

@end
