//
//  XKFaviconManager.h
//  X-Key
//
//  Created by Chang Shu on 3/25/14.
//
//

#import <Foundation/Foundation.h>

@interface XKFaviconManager : NSObject <NSURLConnectionDelegate>
{
    CFMutableDictionaryRef _responseDataMap; // keyed by a connection
    NSMutableDictionary *_connectonMap; // keyed by urlstring
    NSMutableDictionary *_faviconMap; // keyed by urlstring
    NSMutableDictionary *_loadedFaviconMap; // keyed by urlstring
    id _controller;
}

- (UIImage*)retrieveFavicon:(NSString*)urlString;
- (void)removeFavicon:(NSString*)urlString;

+ (XKFaviconManager *)loadFavicons;
- (void)saveFavicons;
- (void)setController:(id)controller;

@end
