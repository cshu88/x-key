//
//  AppDelegate.h
//  X-Key
//
//  Created by Yi Shen - on 2/28/14.
//
//

#import <UIKit/UIKit.h>

#import "XKKeyList.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property XKKeyList *keyList;

@end

extern AppDelegate* thisApp;
