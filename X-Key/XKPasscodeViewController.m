//
//  XKPasscodeViewController.m
//  X-Key
//
//  Created by Chang Shu on 3/9/14.
//
//

#import "XKPasscodeViewController.h"

#import "AppDelegate.h"
#import "XKMessageView.h"

enum PasscodeViewControllerState {
    PasscodeViewControllerStateEnterCurrent = 0,
    PasscodeViewControllerStateEnterNew = 1,
    PasscodeViewControllerStateEnterConfirm = 2
};

@interface XKPasscodeViewController ()

@property (weak, nonatomic) IBOutlet UINavigationItem *barTitle;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *passcodeLabel;
@property (weak, nonatomic) IBOutlet UITextField *passcodeField;

@property (weak, nonatomic) IBOutlet UIButton *fakeDoneButton;
@property (strong, nonatomic) IBOutlet UIToolbar* keyboardToolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property enum PasscodeViewControllerState state;
@property NSString* futurePasscodeString1;
@property NSString* futurePasscodeString2;

@end

@implementation XKPasscodeViewController

int max_textLength = 20;

- (BOOL)isValidPasscode:(NSString*)passcode
{
    if (passcode.length >= 4) {
        return YES;
    }
    return NO;
}

- (void)popupAlert:(NSString*)message
{
    XKMessageView* alert = [[XKMessageView alloc] initWithText:message inView:self.view];
    [alert showAndDismissAfter:1.6];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger newLength = textField.text.length + string.length - range.length;
    return (newLength > max_textLength) ? NO : YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self shouldPerformSegueWithIdentifier:@"unwindFromDone" sender:self.doneButton]) {
        [self performSegueWithIdentifier:@"unwindFromDone" sender:self.doneButton];
        return YES;
    }
    return NO;
}

-(void)donePressed
{
    [self textFieldShouldReturn:_passcodeField];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.inputAccessoryView = self.keyboardToolbar;
    return YES;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    switch (self.uiType) {
        case PasscodeViewControllerTypeCreate:
            if (sender == self.doneButton) {
                if (self.state == PasscodeViewControllerStateEnterNew) {
                    self.futurePasscodeString1 = self.passcodeField.text;
                    if (![self isValidPasscode:self.futurePasscodeString1]) {
                        [self popupAlert:NSLocalizedString(@"Use at least 4 digits.", @"message")];
                        self.passcodeField.text = @"";
                    } else {
                        self.passcodeLabel.text = NSLocalizedString(@"Confirm passcode", @"label");
                        self.doneButton.title = NSLocalizedString(@"Done", @"done button");
                        self.passcodeField.text = @"";
                        self.state = PasscodeViewControllerStateEnterConfirm;
                    }
                } else if (self.state == PasscodeViewControllerStateEnterConfirm) {
                    self.futurePasscodeString2 = self.passcodeField.text;
                    if (![self.futurePasscodeString1 isEqualToString:self.futurePasscodeString2]) {
                        [self popupAlert:NSLocalizedString(@"Passcodes didn't match.", @"message")];
                        self.passcodeLabel.text = NSLocalizedString(@"Enter new passcode", @"label");
                        self.doneButton.title = NSLocalizedString(@"Next", @"next button");
                        self.passcodeField.text = @"";
                        self.state = PasscodeViewControllerStateEnterNew;
                    } else {
                        self.keyList.passcode = self.futurePasscodeString1;
                        self.keyList.state = XKKeyListStateValid;
                        return YES;
                    }
                }
            } else if (sender == self.cancelButton) {
                self.keyList.state = XKKeyListStateInvalid;
                return YES;
            }
            break;
        case PasscodeViewControllerTypeEnter:
            if (sender == self.doneButton) {
                self.keyList = [XKKeyList loadAndDecryptKeys:self.passcodeField.text];
                if (self.keyList.state == XKKeyListStateValid) {
                    return YES;
                } else {
                    [self popupAlert:NSLocalizedString(@"Invalid Passcode. Try again.", @"message")];
                    self.passcodeField.text = @"";
                }
            } else if (sender == self.cancelButton) {
                self.keyList.state = XKKeyListStateInvalid;
                self.keyList.passcode = self.passcodeField.text;
                return YES;
            }
            break;
        case PasscodeViewControllerTypeUpdate:
            if (sender == self.doneButton) {
                if (self.state == PasscodeViewControllerStateEnterCurrent) {
                    self.keyList = [XKKeyList loadAndDecryptKeys:self.passcodeField.text];
                    if (self.keyList.state == XKKeyListStateValid) {
                        self.passcodeLabel.text = NSLocalizedString(@"Enter new passcode", @"label");
                        self.state = PasscodeViewControllerStateEnterNew;
                    } else {
                        [self popupAlert:NSLocalizedString(@"Invalid Passcode. Try again.", @"message")];
                    }
                    self.passcodeField.text = @"";
                } else if (self.state == PasscodeViewControllerStateEnterNew) {
                    self.futurePasscodeString1 = self.passcodeField.text;
                    if (![self isValidPasscode:self.futurePasscodeString1]) {
                        [self popupAlert:NSLocalizedString(@"Use at least 4 digits.", @"message")];
                        self.passcodeField.text = @"";
                    } else {
                        self.passcodeLabel.text = NSLocalizedString(@"Confirm passcode", @"label");
                        self.doneButton.title = NSLocalizedString(@"Done", @"done button");
                        self.passcodeField.text = @"";
                        self.state = PasscodeViewControllerStateEnterConfirm;
                    }
                } else if (self.state == PasscodeViewControllerStateEnterConfirm) {
                    self.futurePasscodeString2 = self.passcodeField.text;
                    if (![self.futurePasscodeString1 isEqualToString:self.futurePasscodeString2]) {
                        [self popupAlert:NSLocalizedString(@"Passcodes didn't match.", @"message")];
                        self.passcodeLabel.text = NSLocalizedString(@"Enter new passcode", @"label");
                        self.doneButton.title = NSLocalizedString(@"Next", @"next button");
                        self.passcodeField.text = @"";
                        self.state = PasscodeViewControllerStateEnterNew;
                    } else {
                        self.keyList.passcode = self.futurePasscodeString1;
                        self.keyList.state = XKKeyListStateValid;
                        return YES;
                    }
                }
            } else if (sender == self.cancelButton) {
                self.keyList.state = XKKeyListStateInvalid;
                return YES;
            }
            break;
        default:
            break;
    }
    return NO;
}

- (void)toggleBarButton:(bool)show forButton:(UIBarButtonItem *)btn WithTitle:(NSString*)title
{
    if (show) {
        btn.style = UIBarButtonItemStylePlain;
        btn.enabled = true;
        btn.title = title;
    } else {
        btn.style = UIBarButtonItemStylePlain;
        btn.enabled = false;
        btn.title = nil;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
    self.fakeDoneButton.hidden = YES;
    self.keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 36)];
    self.keyboardToolbar.barStyle = UIBarStyleDefault;
    self.doneButton = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done", @"done button") style:UIBarButtonItemStyleDone target:self action:@selector(donePressed)];
    [self.keyboardToolbar setItems: [NSArray arrayWithObjects:
                                //                                [[UIBarButtonItem alloc]initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(tryResignKeyboard)],
                                //                                [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(tryResignKeyboard)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                self.doneButton,
                                nil]];

    self.passcodeField.borderStyle = UITextBorderStyleRoundedRect;
    self.passcodeField.leftViewMode = UITextFieldViewModeAlways;
    UIImageView *lockIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lock36.png"]];
    self.passcodeField.leftView = lockIcon;
    self.passcodeField.delegate = self;
    self.navigationItem.leftBarButtonItem = nil;

    self.keyList = [[XKKeyList alloc] init];
    switch (self.uiType) {
        case PasscodeViewControllerTypeCreate:
            self.barTitle.title = NSLocalizedString(@"Create Passcode", @"view title");
            self.passcodeLabel.text = NSLocalizedString(@"Enter new passcode", @"label");
            self.doneButton.title = NSLocalizedString(@"Next", @"next button");
            self.state = PasscodeViewControllerStateEnterNew;
            [self toggleBarButton:NO forButton:self.cancelButton WithTitle:nil];
            break;
        case PasscodeViewControllerTypeEnter:
            self.barTitle.title = NSLocalizedString(@"Enter Passcode", @"view title");
            self.doneButton.title = NSLocalizedString(@"Done", @"done button");
            self.passcodeLabel.text = NSLocalizedString(@"Enter your passcode", @"label");
            [self toggleBarButton:NO forButton:self.cancelButton WithTitle:nil];
            break;
        case PasscodeViewControllerTypeUpdate:
            self.barTitle.title = NSLocalizedString(@"Reset Passcode", @"view title");
            self.doneButton.title = NSLocalizedString(@"Next", @"next button");
            self.passcodeLabel.text = NSLocalizedString(@"Enter your current passcode", @"label");
            self.state = PasscodeViewControllerStateEnterCurrent;
            [self toggleBarButton:YES forButton:self.cancelButton WithTitle:NSLocalizedString(@"Cancel", @"cancel button")];
            break;
        default:
            break;
    }

    [self.passcodeField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
