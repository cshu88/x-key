//
//  XKKeyList.h
//  X-Key
//
//  Created by Chang Shu on 3/8/14.
//
//

#import <Foundation/Foundation.h>

#import "XKKey.h"

extern NSString * const kEncrytedKeysFileName;

enum XKKeyListState {
    XKKeyListStateInvalid = 0,
    XKKeyListStateValid = 1,
};

@interface XKKeyList : NSObject <NSCoding>

@property NSString *passcode;
@property enum XKKeyListState state;

+ (XKKeyList *)loadAndDecryptKeys:(NSString*)passcode;
- (void)encryptAndSaveKeys;
- (BOOL)checkExistence:(NSString*)name;
- (NSInteger)countOfKeys;
- (XKKey*)getKeyByName:(NSString*)name;
- (XKKey*)getKeyByIndex:(NSInteger)index;
- (void)removeKeyByIndex:(NSInteger)index;
- (void)addNewKey:(XKKey*)key;
- (void)replaceOldKey:(XKKey*)oldKey With:(XKKey*)key;

- (NSInteger)countOfFilteredKeys;
- (void)generateFilteredKeysWithSearchText:(NSString*)searchText;
- (XKKey*)getFilteredKeyByIndex:(NSInteger)index;
- (void)deleteDataFile;

@end
