//
//  XKKeyList.m
//  X-Key
//
//  Created by Chang Shu on 3/8/14.
//
//

#import "XKKeyList.h"

#import "RNDeCryptor.h"
#import "RNEnCryptor.h"
#import "XKKey.h"

NSString * const kNumberOfKeys = @"XKKeyNumberOfKeys";
NSString * const kEncrytedKeysFileName = @"Documents/XKEncryptedKeys.bin";

@interface XKKeyList ()

@property NSMutableArray *keys;

@property NSMutableArray *filteredKeys;

@end

@implementation XKKeyList

- (id)init
{
    self = [super init];
    self.passcode = @"ABC123";
    self.keys = [[NSMutableArray alloc] init];
    self.state = XKKeyListStateInvalid;
    self.filteredKeys = [NSMutableArray arrayWithCapacity:[self.keys count]];
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [self init];
    if (!self)
        return nil;
    int numberOfKeys = 0;//[coder decodeIntForKey:kNumberOfKeys];
    while (1) {
        XKKey *key = [coder decodeObjectForKey:[NSString stringWithFormat:@"%d", numberOfKeys]];
        if (key == nil)
            break;
        [self.keys addObject:key];
        numberOfKeys++;
    }
    self.state = XKKeyListStateInvalid;
    self.filteredKeys = [NSMutableArray arrayWithCapacity:[self.keys count]];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
//    [coder encodeInt:[self.keys count] forKey:kNumberOfKeys];
    for (int i = 0; i < [self.keys count]; i++) {
        XKKey * key = self.keys[i];
        [coder encodeObject:key forKey:[NSString stringWithFormat:@"%d", i]];
    }
}

- (NSData*)serializeData
{
    return [NSKeyedArchiver archivedDataWithRootObject:self];
}

+ (XKKeyList *)deSerializeData:(NSData*)serializedData
{
    XKKeyList * keyList = nil;
    if (serializedData) {
        @try {
            keyList = [NSKeyedUnarchiver unarchiveObjectWithData:serializedData];
            if (![keyList isKindOfClass:[XKKeyList class]]) {
                keyList = nil;
            }
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
            keyList = nil;
        }
    }
    return keyList;
}

- (void)deleteDataFile
{
    NSString  *encryptedFilePath = [NSHomeDirectory() stringByAppendingPathComponent:kEncrytedKeysFileName];
    [[NSFileManager defaultManager] removeItemAtPath:encryptedFilePath error:nil];
}

- (void)encryptAndSaveKeys
{
    if (self.state != XKKeyListStateValid)
        return;
    NSString  *encryptedFilePath = [NSHomeDirectory() stringByAppendingPathComponent:kEncrytedKeysFileName];
    
    NSData *data = [self serializeData];
    NSError *error = nil;
    NSData *encryptedData = [RNEncryptor encryptData:data
                                        withSettings:kRNCryptorAES256Settings
                                            password:self.passcode
                                               error:&error];
    [encryptedData writeToFile:encryptedFilePath atomically:YES];
}

+ (XKKeyList *)loadAndDecryptKeys:(NSString*)passcode
{
    XKKeyList *keyList = nil;
    if (passcode != nil) {
        NSString  *encryptedFilePath = [NSHomeDirectory() stringByAppendingPathComponent:kEncrytedKeysFileName];
        NSData *encryptedData = [NSData dataWithContentsOfFile:encryptedFilePath];
        if (encryptedData) {
            NSError *error = nil;
            NSData *decryptedData = [RNDecryptor decryptData:encryptedData
                                                withSettings:kRNCryptorAES256Settings
                                                    password:passcode
                                                       error:&error];
            keyList = [self deSerializeData:decryptedData];
            if (keyList) {
                keyList.passcode = passcode;
                keyList.state = XKKeyListStateValid;
            }
        }
    }
    if (!keyList) {
        keyList = [[XKKeyList alloc] init];
    }
    return keyList;
}

- (BOOL)checkExistence:(NSString*)name
{
    for (int i = 0; i < [self.keys count]; i++) {
        XKKey * key = self.keys[i];
        if ([[key getField:kKEYNAMEKEY] isEqualToString:name]) {
            return YES;
        }
    }
    return NO;
}

- (NSInteger)countOfKeys
{
    return [self.keys count];
}

- (XKKey*)getKeyByName:(NSString*)name
{
    for (int i = 0; i < [self.keys count]; i++) {
        XKKey * key = self.keys[i];
        if ([[key getField:kKEYNAMEKEY] isEqualToString:name]) {
            return key;
        }
    }
    return nil;
}

- (XKKey*)getKeyByIndex:(NSInteger)index
{
    return self.keys[index];
}

- (void)removeKeyByIndex:(NSInteger)index
{
    [self.keys removeObjectAtIndex:index];
    self.filteredKeys = [NSMutableArray arrayWithCapacity:[self.keys count]];
}

- (void)addNewKey:(XKKey*)key
{
    [self.keys insertObject:key atIndex:0];
    self.filteredKeys = [NSMutableArray arrayWithCapacity:[self.keys count]];
}

- (void)replaceOldKey:(XKKey*)oldKey With:(XKKey*)key
{
    for (int i = 0; i < [self.keys count]; i++) {
        if (oldKey == self.keys[i]) {
            self.keys[i] = key;
            return;
        }
    }
    assert(0);
}

- (void)generateFilteredKeysWithSearchText:(NSString*)searchText
{
    if ([searchText length] == 0) {
        self.filteredKeys = [NSMutableArray arrayWithArray:self.keys];
        return;
    }
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [self.filteredKeys removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.searchable contains[c] %@",searchText];
    self.filteredKeys = [NSMutableArray arrayWithArray:[self.keys filteredArrayUsingPredicate:predicate]];
}

- (NSInteger)countOfFilteredKeys
{
    return [self.filteredKeys count];
}

- (XKKey*)getFilteredKeyByIndex:(NSInteger)index
{
    if (index < self.filteredKeys.count) {
        return self.filteredKeys[index];
    }
    return nil;
}

@end
