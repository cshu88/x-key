//
//  XKEditKeyViewController.h
//  X-Key
//
//  Created by Chang Shu on 3/6/14.
//
//

#import <UIKit/UIKit.h>

#import "XKKey.h"

enum EditKeyViewControllerType {
    EditKeyViewControllerTypeAdd = 0,
    EditKeyViewControllerTypeEdit = 1,
    EditKeyViewControllerTypeReview = 2,
};

@interface XKEditKeyViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate>

@property XKKey *oldKey;
@property XKKey *key;
@property (nonatomic) IBOutlet UIScrollView *scrollView;
//@property UIBarButtonItem* doneButton;
@property enum EditKeyViewControllerType uiType;

@end
