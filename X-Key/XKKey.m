//
//  XKKey.m
//  X-Key
//
//  Created by Chang Shu on 3/6/14.
//
//

#import "XKKey.h"

NSString * const kKEYNAMEKEY = @"keyname";
NSString * const kURLKEY = @"URL";
NSString * const kUSERNAMEKEY = @"username";
NSString * const kPASSWORDKEY = @"password";
NSString * const kCOMMENTSKEY = @"comments";
NSString * const kLASTUPDATETIMEKEY = @"lastupdatetime";
NSString * const kLASTUPDATEDATEONLYKEY = @"lastupdatedateonly";

@interface XKKey ()
@property NSString *name;
@property NSDate *lastUpdateTime;
@property NSMutableDictionary *fields;
@property NSMutableString *searchable;
@end

@implementation XKKey

- (id)init {
    self = [super init];
    self.name = [[NSString alloc] init];
    self.lastUpdateTime = [[NSDate alloc] init];
    self.fields = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                   kURLKEY, @".com",
                   kUSERNAMEKEY, @"",
                   kPASSWORDKEY, @"",
                   kCOMMENTSKEY, @"",
                   Nil];
    return self;
}

- (void)makeSearable {
    self.searchable = [[NSMutableString alloc] initWithString:self.name];
    NSString* value = [self getField:kURLKEY];
    if (value) {
        [self.searchable appendString:@" "];
        [self.searchable appendString:value];
    }
    value = [self getField:kUSERNAMEKEY];
    if (value) {
        [self.searchable appendString:@" "];
        [self.searchable appendString:value];
    }
    value = [self getField:kPASSWORDKEY];
    if (value) {
        [self.searchable appendString:@" "];
        [self.searchable appendString:value];
    }
    value = [self getField:kCOMMENTSKEY];
    if (value) {
        [self.searchable appendString:@" "];
        [self.searchable appendString:value];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [self init];
    if (!self)
        return nil;
    self.name = [coder decodeObjectForKey:kKEYNAMEKEY];
    self.lastUpdateTime = [coder decodeObjectForKey:kLASTUPDATETIMEKEY];
    NSString * value = [coder decodeObjectForKey:kURLKEY];
    if (value)
        [self.fields setObject:value forKey:kURLKEY];
    value = [coder decodeObjectForKey:kUSERNAMEKEY];
    if (value)
        [self.fields setObject:value forKey:kUSERNAMEKEY];
    value = [coder decodeObjectForKey:kPASSWORDKEY];
    if (value)
        [self.fields setObject:value forKey:kPASSWORDKEY];
    value = [coder decodeObjectForKey:kCOMMENTSKEY];
    if (value)
        [self.fields setObject:value forKey:kCOMMENTSKEY];

    [self makeSearable];
    return self;
}

- (NSString*)getField:(NSString *)key {
    if ([key isEqualToString:(kKEYNAMEKEY)]) {
        return self.name;
    } else if ([key isEqualToString:kLASTUPDATETIMEKEY]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        return [dateFormatter stringFromDate:self.lastUpdateTime];
    } else if ([key isEqualToString:kLASTUPDATEDATEONLYKEY]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        return [dateFormatter stringFromDate:self.lastUpdateTime];
    } else {
        return self.fields[key];
    }
}

- (void)setField:(NSString *)value forKey:(NSString *)key {
    BOOL updated = NO;
    if ([key isEqualToString:(kKEYNAMEKEY)]) {
        if (![value isEqualToString:self.name]) {
            self.name = value;
            updated = YES;
        }
    } else if ([key isEqualToString:kLASTUPDATETIMEKEY]) {
    } else if ([key isEqualToString:kLASTUPDATEDATEONLYKEY]) {
    } else {
        if (![value isEqualToString:self.fields[key]]) {
            [self.fields setObject:value forKey:key];
            updated = YES;
        }
    }
    if (updated) {
        self.lastUpdateTime = [NSDate date];
        [self makeSearable];
    }
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.name forKey:kKEYNAMEKEY];
    [coder encodeObject:self.lastUpdateTime forKey:kLASTUPDATETIMEKEY];
    for (NSString * fieldName in self.fields) {
        NSString *fieldValue = [self.fields objectForKey:fieldName];
        [coder encodeObject:fieldValue forKey:fieldName];
    }
}

@end
