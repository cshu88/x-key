//
//  XKKeyListViewController.m
//  X-Key
//
//  Created by Chang Shu on 3/6/14.
//
//

#import "XKKeyListViewController.h"

#import "AppDelegate.h"
#import "XKPasscodeViewController.h"
#import "XKEditKeyViewController.h"
#import "XKKey.h"
#import "XKKeyList.h"
#import "XKKeyListCell.h"
#import "XKFaviconManager.h"
#import "XKMessageView.h"

@implementation UIImage (Resize)

- (UIImage*)scaleToSize:(CGSize)size {
    CGFloat scale = MAX(self.size.height/size.height, self.size.height/size.height);
    UIImage* scaledImage = [UIImage imageWithCGImage:self.CGImage scale:scale
                  orientation:UIImageOrientationUp];
    return scaledImage;
}

@end

@interface XKKeyListViewController ()
{
    XKFaviconManager * _faviconMgr;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property NSString *launchNextViewCommand;

@end

@implementation XKKeyListViewController
@synthesize searchController;

static NSInteger kCellNameTag           = 1;
static NSInteger kCellUserTag           = 2;
static NSInteger kCellPasswordTag       = 3;
static NSInteger kCellLastUpdateTimeTag = 4;
static NSInteger kCellFaviconTag        = 5;

- (void)popupAlert:(NSString*)message
{
    XKMessageView* alert = [[XKMessageView alloc] initWithText:message inView:self.navigationController.view];
    [alert showAndDismissAfter:1.0];
}

- (void)launchAboutViewController {
    UIViewController *aboutViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
    [self presentViewController:aboutViewController animated:NO completion:nil];
}

- (void)launchPasscodeViewController:(enum PasscodeViewControllerType)uiType
{
    NSString  *encryptedFilePath = [NSHomeDirectory() stringByAppendingPathComponent:kEncrytedKeysFileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:encryptedFilePath];
    enum PasscodeViewControllerType passcodeUIType = uiType;
    if (!fileExists)
        passcodeUIType = PasscodeViewControllerTypeCreate;

    // Create Passcode
    XKPasscodeViewController *passcodeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PasscodeViewController"];
    passcodeViewController.uiType = passcodeUIType;
    [self presentViewController:passcodeViewController animated:NO completion:nil];
}

- (IBAction)unwindToList:(UIStoryboardSegue *)segue
{
    id source = [segue sourceViewController];
    if ([source isKindOfClass:[XKEditKeyViewController class]]) {
        XKEditKeyViewController * editKeyViewController = source;
        XKKey *key = editKeyViewController.key;
        if (key != nil) {
            if (self.opType == XKKeyListViewControllerOPTypeAdd)
                [thisApp.keyList addNewKey:key];
            else if (self.opType == XKKeyListViewControllerOPTypeUpdate)
                [thisApp.keyList replaceOldKey:editKeyViewController.oldKey With:key];
            [thisApp.keyList encryptAndSaveKeys];
            [self.tableView reloadData];
        }
    } else if ([source isKindOfClass:[XKPasscodeViewController class]]) {
        XKPasscodeViewController * passcodeViewController = source;
        switch (passcodeViewController.uiType) {
            case PasscodeViewControllerTypeCreate:
            case PasscodeViewControllerTypeUpdate:
                if (passcodeViewController.keyList.state == XKKeyListStateValid) {
                    thisApp.keyList = passcodeViewController.keyList;
                    [thisApp.keyList encryptAndSaveKeys];
                }
                break;
            case PasscodeViewControllerTypeEnter:
                if (passcodeViewController.keyList.state == XKKeyListStateValid) {
                    thisApp.keyList = passcodeViewController.keyList;
                }
                break;
            default:
                break;
        }
        [self.tableView reloadData];
    } else if ([segue.identifier isEqualToString:@"segueFromUpdatePasscode"]) { // From AboutViewController
        self.launchNextViewCommand = @"CommandLaunchPasscodeViewController:PasscodeViewControllerTypeUpdate";
    }
}

- (void)prepareForEnteringBackground
{
    [self launchAboutViewController];
    [_faviconMgr saveFavicons];
}

- (void)prepareForEnteringForeground
{
    [self restartView];
}

- (void)restartView
{
    [self.tableView reloadData];
    self.launchNextViewCommand = @"";
    [self launchPasscodeViewController:PasscodeViewControllerTypeEnter];
}

- (void)handleSwipeRight:(UISwipeGestureRecognizer *)recognizer {
    [self popupAlert:NSLocalizedString(@"Swipe Left to Delete", @"help message")];
}

- (void)viewDidLoad
{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    
    self.searchController.searchBar.delegate = self;
    
    self.searchController.searchBar.barTintColor = [UIColor orangeColor];
    [self.tableView setTableHeaderView:self.searchController.searchBar];
    self.definesPresentationContext = YES;

    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    _faviconMgr = [XKFaviconManager loadFavicons];
    [_faviconMgr setController:self];

    UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(handleSwipeRight:)];
    
    [swipeRightGesture setDirection: UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeRightGesture];

    [self restartView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([self.launchNextViewCommand isEqualToString:@"CommandLaunchPasscodeViewController:PasscodeViewControllerTypeUpdate"])
        [self launchPasscodeViewController:PasscodeViewControllerTypeUpdate];
    self.launchNextViewCommand = @"";
}

- (void)viewWillDisappear:(BOOL)animated
{
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView) {
        return [thisApp.keyList countOfKeys];
    }
    return [thisApp.keyList countOfFilteredKeys];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ListPrototypeCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    XKKey *key;
    if (self.searchController.isActive) {
        key = [thisApp.keyList getFilteredKeyByIndex:indexPath.row];
    } else if (tableView == self.tableView) {
        key = [thisApp.keyList getKeyByIndex:indexPath.row];
    }
    UILabel * name = (UILabel *)[cell viewWithTag:kCellNameTag];
    name.text = [key getField:kKEYNAMEKEY];

    NSString * value = [key getField:kUSERNAMEKEY];
    if (!value) {
        value = @"";
    }
    UILabel * user = (UILabel *)[cell viewWithTag:kCellUserTag];
    user.text = value;
    value = [key getField:kPASSWORDKEY];
    if (!value) {
        value = @"";
    }
    UILabel * password = (UILabel *)[cell viewWithTag:kCellPasswordTag];
    password.text = value;
    UILabel * lastUpdateTime = (UILabel *)[cell viewWithTag:kCellLastUpdateTimeTag];
    lastUpdateTime.text = [key getField:kLASTUPDATEDATEONLYKEY];

    NSString *url = [key getField:kURLKEY];
    UIImageView *favicon = (UIImageView *)[cell viewWithTag:kCellFaviconTag];
    favicon.contentMode = UIViewContentModeCenter;
    UIImage *image = [_faviconMgr retrieveFavicon:url];
    favicon.image = [image scaleToSize:CGSizeMake(16, 16)];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    XKEditKeyViewController *destView = [segue destinationViewController];
    if (sender == self.addButton) {
        destView.uiType = EditKeyViewControllerTypeAdd;
        self.opType = XKKeyListViewControllerOPTypeAdd;
    } else if ([sender isKindOfClass:[UITableViewCell class]]) {
        UITableViewCell *cell = sender;
        UILabel *name = (UILabel*)[cell viewWithTag:kCellNameTag];
        destView.key = [thisApp.keyList getKeyByName:name.text];
        destView.uiType = EditKeyViewControllerTypeReview;
        self.opType = XKKeyListViewControllerOPTypeUpdate;
    }
}

#pragma mark - Swipe Left to Delete

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // If row is deleted, remove it from the list.
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [thisApp.keyList removeKeyByIndex:indexPath.row];
        [thisApp.keyList encryptAndSaveKeys];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark -
#pragma mark UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)_searchController
{
    NSString *searchString = _searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self updateSearchResultsForSearchController:self.searchController];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [self updateSearchResultsForSearchController:self.searchController];
}

// Extra method
- (void)searchForText:(NSString *)searchString
{
    /* Put here everything that is in the method:
     - (BOOL)searchDisplayController:(UISearchDisplayController *)controller     shouldReloadTableForSearchString:(NSString *)searchString
     ...BUT WITH NO RETURN VALUE */
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:nil];
}

#pragma mark -
#pragma mark UISearchControllerDelegate

- (void)willPresentSearchController:(UISearchController *)searchController
{
    //..
}
- (void)didPresentSearchController:(UISearchController *)searchController
{
    //..
}
- (void)willDismissSearchController:(UISearchController *)searchController
{
    //..
}
- (void)didDismissSearchController:(UISearchController *)searchController
{
    //..
}

-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [thisApp.keyList generateFilteredKeysWithSearchText:searchText];
}

/* old code */
#if 0
-(BOOL)searchController:(UISearchController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:nil];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}
#endif
-(BOOL)searchController:(UISearchController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchBar.text scope:
     [[self.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}
#if 0
- (void)updateSearchResultsForSearchController:(nonnull UISearchController *)searchController {
    return;
}
#endif

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    return;
}

- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
    return;
}

- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
    return;
}

- (CGSize)sizeForChildContentContainer:(nonnull id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize {
    return parentSize;
}

- (void)systemLayoutFittingSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
    return;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
    return;
}

- (void)willTransitionToTraitCollection:(nonnull UITraitCollection *)newCollection withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
    return;
}

- (void)didUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator {
    return;
}

- (void)setNeedsFocusUpdate {
    return;
}

- (BOOL)shouldUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context {
    return true;
}

- (void)updateFocusIfNeeded {
    return;
}

@end
