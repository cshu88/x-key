//
//  XKKeyListCell.h
//  X-Key
//
//  Created by Chang Shu on 3/20/14.
//
//

#import <UIKit/UIKit.h>

@interface XKKeyListCell : UITableViewCell

- (void)initColumnNumber:(int)numberOfColumns;
- (void)setColumn:(int)column WithLabel:(NSString*)text;

@end
