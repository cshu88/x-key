//
//  X_KeyTests.m
//  X-KeyTests
//
//  Created by Yi Shen on 2/28/14.
//
//

#import <XCTest/XCTest.h>

@interface X_KeyTests : XCTestCase

@end

@implementation X_KeyTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

// Test cases
// 1. create passcode
// 1.1. start from scratch or enter "31415926535" for passcode
// 1.2. check create passcode view
// 1.3. enter "123" and tap "next". check prompt msg: "Use at least 4 digits"
// 1.4. enter "1111" and tap "next". check view changed
// 1.5. enter "1234" and tap "done". check prompt msg: "Passcode didn't match"
// 1.6. re-enter "1111" and tap "next". enter "1111" and tap "done". check key listview displayed.

// 2. enter passcode
// 2.1 restart app. check enter passcode view
// 2.2 enter "123" and tap "done". check msg: "Invalid passcode. Try again."
// 2.3 enter "1111" and tap "done". check keylist view displayed.

// 3. add key
// 3.1 tap '+" and check 'add key' view loaded. focus is in 'name' field
// 3.2 type any name in the name field. try an existing name to see error msg
// 3.3 continue enter url, user, password, comments, etc. check auto-position
// 3.4 tap save. check back to key list view. new entry displayed in 1st row
// 3.5 tap '+' again. enter any data and tap 'cancel'. check no new entry

// 4. edit key
// 4.1 
@end
