//
//  XKEditKeyViewController.m
//  X-Key
//
//  Created by Chang Shu on 3/6/14.
//
//

#import "XKEditKeyViewController.h"

#import "AppDelegate.h"
#import "XKMessageView.h"

@interface XKEditKeyViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *urlField;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextView *commentView;
@property (weak, nonatomic) IBOutlet UILabel *lastUpdatedLabel;

@property UIView *activeField;
@property CGSize kbSize;
@property NSInteger viewOffsetY;
@property BOOL isVKBShowing;

@end

@implementation XKEditKeyViewController

- (void)populateKey
{
    [self resetMode];
    if (self.key == nil)
        return;
    self.oldKey = self.key;
    self.nameField.text = [self.key getField:kKEYNAMEKEY];
    self.urlField.text = [self.key getField:kURLKEY];
    self.usernameField.text = [self.key getField:kUSERNAMEKEY];
    self.passwordField.text = [self.key getField:kPASSWORDKEY];
    self.commentView.text = [self.key getField:kCOMMENTSKEY];
    self.lastUpdatedLabel.text = NSLocalizedString(@"Last Updated: ", @"last updated label");
    self.lastUpdatedLabel.text = [self.lastUpdatedLabel.text stringByAppendingString:[self.key getField:kLASTUPDATETIMEKEY]];
}

- (void)resetMode
{
    switch (self.uiType) {
        case EditKeyViewControllerTypeAdd:
            [self enterAddMode];
            break;
        case EditKeyViewControllerTypeEdit:
            [self enterEditMode];
            break;
        case EditKeyViewControllerTypeReview:
            [self enterReviewMode];
            break;
        default:
            break;
    }
}

- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

- (void)enterAddMode
{
    [self backViewController].title = NSLocalizedString(@"Cancel", @"cancel button");
    self.navigationItem.title = NSLocalizedString(@"Add Key", @"Add Key view title");
    self.doneButton.title = NSLocalizedString(@"Save", @"save button");
    self.navigationItem.leftBarButtonItem = nil;//self.cancelButton;
//    self.nameField.text = @"Add Key Name";
    [self.nameField becomeFirstResponder];
    [self.lastUpdatedLabel setHidden:YES];
    //[UIMenuController sharedMenuController].menuVisible = NO;
}

- (void)enterEditMode
{
    [self backViewController].title = NSLocalizedString(@"Cancel", @"cancel button");
    self.navigationItem.title = NSLocalizedString(@"Edit Key", @"Edit Key view title");
    self.doneButton.title = NSLocalizedString(@"Save", @"save button");
    self.navigationItem.leftBarButtonItem = nil;//self.cancelButton;
    self.navigationItem.rightBarButtonItem = self.doneButton;

    self.urlField.textColor = [UIColor blackColor];
    self.usernameField.textColor = [UIColor blackColor];
    self.passwordField.textColor = [UIColor blackColor];
    self.commentView.textColor = [UIColor blackColor];
    [self.lastUpdatedLabel setHidden:YES];
}

- (void)enterReviewMode
{
    [self backViewController].title = NSLocalizedString(@"Key List", @"Key List view title");
    // remove focus
    [self dismissVKB];
    self.navigationItem.title = NSLocalizedString(@"View Key", @"View Key view title");
    self.doneButton.title = NSLocalizedString(@"Edit", @"edit button");
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = self.doneButton;
    
    self.urlField.textColor = [UIColor grayColor];
    self.usernameField.textColor = [UIColor grayColor];
    self.passwordField.textColor = [UIColor grayColor];
    self.commentView.textColor = [UIColor grayColor];
    [self.lastUpdatedLabel setHidden:NO];
}

- (void) dismissVKB
{
    if (self.nameField.isFirstResponder)
        [self.nameField resignFirstResponder];
    else if (self.urlField.isFirstResponder)
        [self.urlField resignFirstResponder];
    else if (self.usernameField.isFirstResponder)
        [self.usernameField resignFirstResponder];
    else if (self.passwordField.isFirstResponder)
        [self.passwordField resignFirstResponder];
    [self.commentView endEditing:YES];
}

- (void)saveKey
{
    assert(self.nameField.text.length > 0);

    self.oldKey = self.key;
    self.key = [[XKKey alloc] init];
    [self.key setField:self.nameField.text forKey:kKEYNAMEKEY];
    if (self.urlField.text.length > 0)
        [self.key setField:self.urlField.text forKey:kURLKEY];
    if (self.usernameField.text.length > 0)
        [self.key setField:self.usernameField.text forKey:kUSERNAMEKEY];
    if (self.passwordField.text.length > 0)
        [self.key setField:self.passwordField.text forKey:kPASSWORDKEY];
    if (self.commentView.text.length > 0)
        [self.key setField:self.commentView.text forKey:kCOMMENTSKEY];
}

#if 0
- (void)handleAlertView:(UIAlertController*)alert
{
    [alert dismissWithClickedButtonIndex:-1 animated:YES];
}
#endif

- (void)popupAlert:(NSString*)message atDelay:(double)delay
{
    XKMessageView* alert = [[XKMessageView alloc] initWithText:message inView:self.navigationController.view];
    [alert showAndDismissAfter:delay];
#if 0
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:0 cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [alert show];
    [self performSelector:@selector(handleAlertView:) withObject:alert afterDelay:delay];
#endif
}

- (BOOL)validateContent
{
    switch (self.uiType) {
        case EditKeyViewControllerTypeAdd:
            if (self.nameField.text.length <= 0) {
                [self popupAlert:NSLocalizedString(@"Key name is invalid.", @"Key name is invalid message") atDelay:1.6];
                return NO;
            }
            
            if ([thisApp.keyList checkExistence:self.nameField.text]) {
                [self popupAlert:NSLocalizedString(@"Key already exists.", @"Key already exists message") atDelay:1.6];
                return NO;
            }
            break;
        case EditKeyViewControllerTypeEdit:
            if (self.nameField.text.length <= 0) {
                [self popupAlert:NSLocalizedString(@"Key name is invalid.", @"Key name is invalid message") atDelay:1.6];
                return NO;
            }
            if (![self.nameField.text isEqualToString:[self.key getField:kKEYNAMEKEY]]) {
                if ([thisApp.keyList checkExistence:self.nameField.text]) {
                    [self popupAlert:NSLocalizedString(@"Key already exists.", @"Key already exists message") atDelay:1.6];
                    return NO;
                }
            }
            break;
        default:
            break;
    }
    return YES;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if (sender == self.cancelButton) {
        if (self.uiType == EditKeyViewControllerTypeAdd) {
            self.key = nil;
            return YES;
        }
        else if (self.uiType == EditKeyViewControllerTypeEdit) {
            self.uiType = EditKeyViewControllerTypeReview;
            [self populateKey];
            return NO;
        } else if  (self.uiType == EditKeyViewControllerTypeReview) {
            if (self.oldKey == self.key)
                self.key = nil;
            return YES;
        }
        return YES;
    } else if (sender == self.doneButton) {
        if (self.uiType == EditKeyViewControllerTypeAdd || self.uiType == EditKeyViewControllerTypeEdit) {
            if (![self validateContent])
                return NO;
            [self saveKey];
            self.uiType = EditKeyViewControllerTypeReview;
            [self resetMode];
            return YES;
        } else if (self.uiType == EditKeyViewControllerTypeReview) {
            self.uiType = EditKeyViewControllerTypeEdit;
            [self resetMode];
            return NO;
        }
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    int max_textLength = 25;
    if (textField == self.urlField) {
        max_textLength = 400;
    } else if (textField == self.nameField) {
        max_textLength = 25;
    }
    if (string.length == 0 && textField.text.length > 0) { // Delete
        return YES;
    }
    NSInteger newLength = textField.text.length + string.length - range.length;
    return (newLength > max_textLength) ? NO : YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    int max_textLength = 400;
    NSInteger newLength = textView.text.length + text.length - range.length;
    return (newLength > max_textLength) ? NO : YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.uiType == EditKeyViewControllerTypeReview) {
        if ([textField.text length] > 0)
            [self copyToPasteboard:textField.text];
        return NO;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (self.uiType == EditKeyViewControllerTypeReview) {
        if ([textView.text length] > 0)
            [self copyToPasteboard:textView.text];
        return NO;
    }
    return YES;
}

- (void)copyToPasteboard:(NSString *)text
{
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    //pb.persistent = YES;
    [pb setString:text];
    [self popupAlert:NSLocalizedString(@"Copied to Clipboard.", @"Copied to Clipboard message") atDelay:1.0];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    if (!parent) {
        [self backViewController].title = NSLocalizedString(@"Key List", @"Key List view title");
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self.scrollView setScrollEnabled:YES];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect scrollFrame = self.scrollView.frame;
    scrollFrame.size.height = screenRect.size.height; // Adjust the height with screen size
    self.scrollView.frame = scrollFrame;
    [self.scrollView setContentSize: screenRect.size];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.

    // Initialize content
    [self populateKey];

    [[self.commentView layer] setBorderColor:[[UIColor colorWithWhite: 0.70 alpha:0.35] CGColor]];
    [[self.commentView layer] setBorderWidth:0.9];
    [[self.commentView layer] setCornerRadius:8];
    self.commentView.delegate = self;

    self.isVKBShowing = NO;
    self.viewOffsetY = self.navigationController.navigationBar.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;

    [self registerForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// For VKB next button
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

// Scroll the text field behind VKB
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeField = textField;
    if (self.isVKBShowing) {
        [self autoPositionActiveField];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.activeField = nil;
}

-(void) textViewDidBeginEditing:(UITextView *)observationComment {
    self.activeField = observationComment;
    if (self.isVKBShowing) {
        [self autoPositionActiveField];
    }
}

-(void) textViewDidEndEditing:(UITextView *)observationComment {
    self.activeField = nil;
}

- (void)autoPositionActiveField {
    if (!self.activeField) {
        return;
    }
    NSInteger offset = self.viewOffsetY;
    self.scrollView.contentInset = UIEdgeInsetsMake(offset, 0.0, self.kbSize.height, 0.0);
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(offset, 0.0, self.kbSize.height, 0.0);

    CGRect aRect = self.view.frame;
    aRect.origin.y += self.viewOffsetY;
    aRect.size.height -= self.kbSize.height + self.viewOffsetY;
    CGRect fieldRect = self.activeField.frame;
    fieldRect.origin.y -= self.scrollView.contentOffset.y;
    if (!CGRectContainsRect(aRect, fieldRect)) {
        [self.scrollView scrollRectToVisible:self.activeField.frame animated:YES];
    }
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    self.isVKBShowing = YES;
    NSDictionary* info = [aNotification userInfo];
    self.kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self autoPositionActiveField];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.viewOffsetY, 0.0, 0.0, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    self.isVKBShowing = NO;
}

@end
