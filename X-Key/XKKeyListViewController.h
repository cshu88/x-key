//
//  XKKeyListViewController.h
//  X-Key
//
//  Created by Chang Shu on 3/6/14.
//
//

#import <UIKit/UIKit.h>
#import "XKKey.h"

enum XKKeyListViewControllerOPType {
    XKKeyListViewControllerOPTypeAdd = 0,
    XKKeyListViewControllerOPTypeUpdate = 1,
    XKKeyListViewControllerOPTypeDelete = 2,
    XKKeyListViewControllerOPTypeNone = 3,
};

@interface XKKeyListViewController : UIViewController<UITableViewDelegate, UISearchBarDelegate, UISearchResultsUpdating, UISearchControllerDelegate>

@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UISearchController *searchController;
@property enum XKKeyListViewControllerOPType opType;

- (IBAction)unwindToList:(UIStoryboardSegue *)segue;

- (void)prepareForEnteringBackground;
- (void)prepareForEnteringForeground;

@end
