//
//  XKKey.h
//  X-Key
//
//  Created by Chang Shu on 3/6/14.
//
//

#ifndef X_Key_XKKey_h
#define X_Key_XKKey_h

extern NSString * const kKEYNAMEKEY;
extern NSString * const kURLKEY;
extern NSString * const kUSERNAMEKEY;
extern NSString * const kPASSWORDKEY;
extern NSString * const kCOMMENTSKEY;
extern NSString * const kLASTUPDATETIMEKEY;
extern NSString * const kLASTUPDATEDATEONLYKEY;

@interface XKKey : NSObject <NSCoding>

- (NSString*)getField:(NSString *)key;
- (void)setField:(NSString *)value forKey:(NSString *)key;

@end

#endif
