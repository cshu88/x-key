//
//  XKPasscodeViewController.m
//  X-Key
//
//  Created by Chang Shu on 3/9/14.
//
//

#import "XKAboutViewController.h"

#import "AppDelegate.h"

@interface XKAboutViewController ()

@end

@implementation XKAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)shareBtnPressed: (id)sender
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        [mailComposer setSubject:NSLocalizedString(@"Try X-Key!", @"mail subject")];
        NSString *message = NSLocalizedString(@"X-Key is a great app that helps you manage all your passwords. You can download it from https://itunes.apple.com/us/app/x-key/id856416637?mt=8", @"mail body");
        [mailComposer setMessageBody:message
                              isHTML:YES];
        mailComposer.mailComposeDelegate = self;
        [self presentViewController:mailComposer animated:YES completion:nil];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result
                       error:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
