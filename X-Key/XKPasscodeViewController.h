//
//  XKPasscodeViewController.h
//  X-Key
//
//  Created by Chang Shu on 3/9/14.
//
//

#import <UIKit/UIKit.h>

#import "XKKeyList.h"

enum PasscodeViewControllerType {
    PasscodeViewControllerTypeEnter = 0,
    PasscodeViewControllerTypeCreate = 1,
    PasscodeViewControllerTypeUpdate = 2
};

@interface XKPasscodeViewController : UIViewController<UITextFieldDelegate>

@property XKKeyList* keyList; //Temporary keylist read from disk when passcode enter

@property enum PasscodeViewControllerType uiType;

@end
